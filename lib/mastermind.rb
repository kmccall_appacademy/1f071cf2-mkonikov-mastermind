class Code
  attr_reader :pegs
  PEGS = { r: 'Red', g: 'Green', b: 'Blue', y: 'Yellow', o: 'Orange', p: 'Purple' }
  LETTERS = ['r','g','b','y','o','p']

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    randomized = []
    4.times { randomized << LETTERS[rand(0..5)] }
    Code.new(randomized)
  end

  def self.parse(input)
    parsed = input.downcase.split("")
    raise "error" if parsed.any? { |el| !LETTERS.include?(el) }
    Code.new(parsed)
  end

  def exact_matches(code)
    count = 0
    (0..3).each do |num|
      count += 1 if code.pegs[num] == self.pegs[num]
    end
    count
  end

  def near_matches(code)
    count = 0
    secret_count = Hash.new(0)
    guess_count = Hash.new(0)
    (0..3).each do |num|
      guess_count[self.pegs[num]] += 1
      secret_count[code.pegs[num]] += 1
    end

    LETTERS.each do |ltr|
      while (secret_count[ltr] > 0) && (guess_count[ltr] > 0)
        count += 1
        secret_count[ltr] -= 1
        guess_count[ltr] -= 1
      end
    end
    count -= self.exact_matches(code)
  end


  def ==(other_code)
    return false unless other_code.is_a?(Code)
    return true if self.pegs == other_code.pegs
  end

  def [](i)
    @pegs[i]
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @plays = 0
  end

  def get_guess
      input = gets.chomp
      Code.parse(input)
  end

  def display_matches(guess)
    puts "Good try! That guess had #{guess.exact_matches(@secret_code)} exact matches"
    puts "It also had #{guess.near_matches(@secret_code)} near matches"
  end

  def play
    until @plays == 10
      puts "Guess the code! -hint: #{@secret_code.pegs}  (Enter your guess by typing the first letter of the 6 colors, e.g. RGBY)"
      guess = get_guess
      display_matches(guess)
      break if guess.exact_matches(@secret_code) == 4
      @plays += 1
    end
    puts "Game over! The code was #{@secret_code.pegs.join.upcase}"
  end
end

#chabad = Code.parse("BbbB")

#charles = Game.new
#charles.play
#p chabad

#p Code.parse("BBBB")
#p Code.parse("bbbb")
